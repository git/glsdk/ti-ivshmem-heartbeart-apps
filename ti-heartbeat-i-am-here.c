#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <time.h>

#define CONFIG_HEARTBEAT_DEVICE "/dev/uio0"
#define CONFIG_INTR_MAP_SIZE (256)

enum ivshmem_registers {
    IntrMask = 0,
    IntrStatus = 4,
    IVPosition = 8,
    Doorbell = 12,
    IVLiveList = 16
};

struct {
	int fd;
	uint8_t *intr_map_addr;
} heartbeat;

static int setup_heartbeat() {
	int ret;

	heartbeat.fd = open(CONFIG_HEARTBEAT_DEVICE, O_RDWR);
	if(heartbeat.fd < 0) {
		printf("fd open: %s\n", strerror(errno));
		return -1;
	}

	heartbeat.intr_map_addr = mmap(0, CONFIG_INTR_MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, heartbeat.fd, 0);
	if(heartbeat.intr_map_addr == MAP_FAILED) {
		printf("intr mmap: %s\n", strerror(errno));
		close(heartbeat.fd);
		return -1;
	}

	return 0;
}

static void fini_heartbeat()
{
	munmap(heartbeat.intr_map_addr, CONFIG_INTR_MAP_SIZE);
	close(heartbeat.fd);
}

static void intr_heartbeat()
{	
        *((uint32_t *)(&heartbeat.intr_map_addr[Doorbell])) = 0;
}

static int read_heartbeat()
{
	int ret;
	uint32_t readbuf;

	ret = read(heartbeat.fd, &readbuf, sizeof(readbuf));
	if(ret <= 0) {
		if(!ret)
			printf("read 0 bytes!\n");
		else
			printf("read: %s\n", strerror(errno));
		return -1;
	}

	return 0;
}

int main()
{
	if(setup_heartbeat()) {
		return -1;
	}

	while(true) {
		read_heartbeat();
		intr_heartbeat();
	}

	fini_heartbeat();

	return 0;
}
